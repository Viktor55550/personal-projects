﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UGeekStore.Migrations
{
    public partial class fortest000000000000 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderDate",
                table: "Orders",
                nullable: false,
                defaultValue: new DateTime(2020, 1, 21, 1, 13, 53, 956, DateTimeKind.Local).AddTicks(7587),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 1, 21, 0, 31, 23, 216, DateTimeKind.Local).AddTicks(6883));

            migrationBuilder.AlterColumn<DateTime>(
                name: "SendTime",
                table: "Messages",
                nullable: false,
                defaultValue: new DateTime(2020, 1, 21, 1, 13, 53, 962, DateTimeKind.Local).AddTicks(8592),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 1, 21, 0, 31, 23, 223, DateTimeKind.Local).AddTicks(277));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderDate",
                table: "Orders",
                nullable: false,
                defaultValue: new DateTime(2020, 1, 21, 0, 31, 23, 216, DateTimeKind.Local).AddTicks(6883),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 1, 21, 1, 13, 53, 956, DateTimeKind.Local).AddTicks(7587));

            migrationBuilder.AlterColumn<DateTime>(
                name: "SendTime",
                table: "Messages",
                nullable: false,
                defaultValue: new DateTime(2020, 1, 21, 0, 31, 23, 223, DateTimeKind.Local).AddTicks(277),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 1, 21, 1, 13, 53, 962, DateTimeKind.Local).AddTicks(8592));
        }
    }
}
