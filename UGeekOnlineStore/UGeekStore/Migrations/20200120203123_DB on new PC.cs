﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace UGeekStore.Migrations
{
    public partial class DBonnewPC : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RegisterDate",
                table: "Users",
                type: "DATE",
                nullable: false,
                defaultValue: new DateTime(2020, 1, 21, 0, 0, 0, 0, DateTimeKind.Local),
                oldClrType: typeof(DateTime),
                oldType: "DATE",
                oldDefaultValue: new DateTime(2019, 9, 9, 0, 0, 0, 0, DateTimeKind.Local));

            migrationBuilder.AlterColumn<bool>(
                name: "Status",
                table: "Suppliers",
                nullable: true,
                defaultValue: true,
                oldClrType: typeof(bool),
                oldDefaultValue: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Salary",
                table: "Shippers",
                type: "decimal",
                nullable: false,
                defaultValue: 8000m,
                oldClrType: typeof(decimal),
                oldDefaultValue: 80000m);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "Products",
                type: "decimal",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0m);

            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDate",
                table: "Products",
                type: "DATE",
                nullable: false,
                defaultValue: new DateTime(2020, 1, 21, 0, 0, 0, 0, DateTimeKind.Local),
                oldClrType: typeof(DateTime),
                oldType: "DATE",
                oldDefaultValue: new DateTime(2019, 9, 9, 0, 0, 0, 0, DateTimeKind.Local));

            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderDate",
                table: "Orders",
                nullable: false,
                defaultValue: new DateTime(2020, 1, 21, 0, 31, 23, 216, DateTimeKind.Local).AddTicks(6883),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2019, 9, 9, 21, 52, 54, 956, DateTimeKind.Local).AddTicks(5935));

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "OrderDetails",
                type: "decimal",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldDefaultValue: 0m);

            migrationBuilder.AlterColumn<DateTime>(
                name: "SendTime",
                table: "Messages",
                nullable: false,
                defaultValue: new DateTime(2020, 1, 21, 0, 31, 23, 223, DateTimeKind.Local).AddTicks(277),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2019, 9, 9, 21, 52, 54, 963, DateTimeKind.Local).AddTicks(7614));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "RegisterDate",
                table: "Users",
                type: "DATE",
                nullable: false,
                defaultValue: new DateTime(2019, 9, 9, 0, 0, 0, 0, DateTimeKind.Local),
                oldClrType: typeof(DateTime),
                oldType: "DATE",
                oldDefaultValue: new DateTime(2020, 1, 21, 0, 0, 0, 0, DateTimeKind.Local));

            migrationBuilder.AlterColumn<bool>(
                name: "Status",
                table: "Suppliers",
                nullable: false,
                defaultValue: true,
                oldClrType: typeof(bool),
                oldNullable: true,
                oldDefaultValue: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Salary",
                table: "Shippers",
                nullable: false,
                defaultValue: 80000m,
                oldClrType: typeof(decimal),
                oldType: "decimal",
                oldDefaultValue: 8000m);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "Products",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal",
                oldDefaultValue: 0m);

            migrationBuilder.AlterColumn<DateTime>(
                name: "AddDate",
                table: "Products",
                type: "DATE",
                nullable: false,
                defaultValue: new DateTime(2019, 9, 9, 0, 0, 0, 0, DateTimeKind.Local),
                oldClrType: typeof(DateTime),
                oldType: "DATE",
                oldDefaultValue: new DateTime(2020, 1, 21, 0, 0, 0, 0, DateTimeKind.Local));

            migrationBuilder.AlterColumn<DateTime>(
                name: "OrderDate",
                table: "Orders",
                nullable: false,
                defaultValue: new DateTime(2019, 9, 9, 21, 52, 54, 956, DateTimeKind.Local).AddTicks(5935),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 1, 21, 0, 31, 23, 216, DateTimeKind.Local).AddTicks(6883));

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "OrderDetails",
                nullable: false,
                defaultValue: 0m,
                oldClrType: typeof(decimal),
                oldType: "decimal",
                oldDefaultValue: 0m);

            migrationBuilder.AlterColumn<DateTime>(
                name: "SendTime",
                table: "Messages",
                nullable: false,
                defaultValue: new DateTime(2019, 9, 9, 21, 52, 54, 963, DateTimeKind.Local).AddTicks(7614),
                oldClrType: typeof(DateTime),
                oldDefaultValue: new DateTime(2020, 1, 21, 0, 31, 23, 223, DateTimeKind.Local).AddTicks(277));
        }
    }
}
